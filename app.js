const express = require("express");
const bodyParser = require("body-parser");
const morgan = require("morgan");
const cors = require("cors");
const posts = require("./controllers/posts");
const app = express();
app.use(morgan("combined"));
app.use(cors());
app.use(bodyParser.json({ type: "*/*", limit: "50mb" }));

const port = 3000;
app.get("/posts/:table", posts.findAll);
app.get("/posts/:table/:id", posts.findOne);
app.post("/posts/:table", posts.addOne);
app.put("/posts/:table/:id", posts.editOne);
app.delete("/posts/:table/:id", posts.delOne);

app.listen(port, () => console.log(`Example app listening on port ${port}!`));

module.exports = {
  app
};
