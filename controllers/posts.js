const config = {
  client: "mysql",
  connection: {
    host: "localhost",
    user: "root",
    password: "password",
    database: "mydb"
  }
};
const knex = require("knex")(config);

exports.findOne = (req, res) => {
  const tableName = req.params.table;

  knex
    .select()
    .from(tableName)
    .where({ id: req.params.id })
    .then(function(response) {
      res.status(200);
      res.send(response);
    })
    .catch(function(e) {
      res.status(500);
      res.send(e);
    });
};

exports.findAll = (req, res) => {
  const tableName = req.params.table;

  knex
    .select()
    .from(tableName)
    .then(function(response) {
      res.send(response);
    })
    .catch(function(e) {
      res.status(500);
      res.send(e);
    });
};

exports.editOne = (req, res) => {
  const tableName = req.params.table;

  knex
    .select()
    .from(tableName)
    .where({ id: req.params.id })
    .then(function(rows) {
      if (rows.length > 0) {
        return knex
          .table(tableName)
          .update(req.body)
          .where({ id: req.params.id })
          .then(function(affectedRows) {
            res.status(200);
            res.json({
              status: "ok",
              message: `Dane rekordu ${req.params.id} zmienione`
            });
          });
      }
      res.status(404);
      res.send("Brak rekordu o podanym ID");
    })
    .catch(function(e) {
      res.status(500);
      res.send(e);
    });
};

exports.addOne = (req, res) => {
  const tableName = req.params.table;
  const values = req.body;

  knex
    .insert(values)
    .into(tableName)
    .then(function(response) {
      res.status(200);
      res.send(response);
    })
    .catch(function(e) {
      res.status(500);
      res.send(e);
    });
};

exports.delOne = (req, res) => {
  const tableName = req.params.table;

  knex
    .select()
    .from(tableName)
    .where({ id: req.params.id })
    .then(function(rows) {
      if (rows.length > 0) {
        return knex
          .delete()
          .from(tableName)
          .where({ id: req.params.id })
          .then(function(affectedRows) {
            res.status(200);
            res.json({
              status: "ok",
              message: `Rekord o id  ${req.params.id} usunięty`
            });
          });
      }
      res.status(404);
      res.send("Brak rekordu o podanym ID");
    })
    .catch(function(e) {
      res.status(500);
      res.send(e);
    });
};
