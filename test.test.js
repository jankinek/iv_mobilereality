const request = require("supertest");
const { app } = require("./app");
let rowNum = "";
test("Supertest  get all", done => {
  request(app)
    .get("/posts/posts")
    .expect(body => {
      expect(body.body.length > 0).toBe(true);
    })
    .expect(200, done);
});

test("Supertest get one", done => {
  request(app)
    .get("/posts/posts/1")
    .expect(200, done);
});

test("Supertest add one", done => {
  request(app)
    .post("/posts/posts")
    .send({ title: "testing post req", body: "test body3" })
    .expect(body => {
      console.log("body", body.body);
      rowNum = body.body[0];
    })
    .expect(200, done);
});

test("Supertest update one", done => {
  request(app)
    .put("/posts/posts/" + rowNum)
    .send({ title: "test update req", body: "test body3" })
    .expect(200, done);
});
test("Supertest delete one", done => {
  request(app)
    .delete("/posts/posts/" + rowNum)
    .expect(200, done);
});

afterAll(() => {
  console.log("xd");
  //app.close();
});
